FROM openjdk:8-alpine AS builder
WORKDIR /

FROM flink:alpine

ENV JOB_MANAGER_HOST=flink-jobmanager
ENV JOB_MANAGER_PORT=6123


COPY target/etp-ignite-3.0.jar app.jar

CMD flink run -m $JOB_MANAGER_HOST:$JOB_MANAGER_PORT -c com.etp.stream.flink.kafka.Kafka010Example app.jar